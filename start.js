/*eslint semi: ["warn", "never"]*/
/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */

//import _ from 'underscore'
import tracer from 'tracer'
import marked from 'marked'
import express from 'express'
import { generate } from 'generate-password'
import bodyParser from 'body-parser'
import { getScreenshot } from './chrome-client'
import path from 'path'

const level = process.env.LOG_LEVEL || 'warn'
const tracerOptions = process.env.DEBUG === 'true' ? { level } : { format: "{{message}}", level }
const logger = tracer.colorConsole( tracerOptions )

// ------------------------------
// express setting
// ------------------------------
const app = express()

// parse application/x-www-form-urlencoded
// app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

const db = {}

const generateHtml = ( content ) => `
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/frameworks-4ba00b1aa0227e4b7a7961544c3b7938afb2720757a471735991ec4475c829e0.css" integrity="sha256-S6ALGqAifkt6eWFUTDt5OK+ycgdXpHFzWZHsRHXIKeA=" media="all" rel="stylesheet" />
  <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/github-d5b96b259d9b0c75f0bdf46a469ce6d35a33b3b53ad365d5b5a5c1f55950ff0c.css" integrity="sha256-1blrJZ2bDHXwvfRqRpzm01ozs7U602XVtaXB9VlQ/ww=" media="all" rel="stylesheet" />

  <title>markdown renderer</title>
</head>
  <body>
    <div class="markdown-body" style="margin: 20px;">
      ${content}
    </div>
  </body>
</html>
`

app.use('/screenshot', express.static('screenshot'))

app.post('/api/render/markdown', (req, res) => {
  logger.warn( req.body  )
  res.setHeader('Content-Type', 'application/json')
  // test
  // curl -X POST -H "Content-Type: application/json" -d '{ "markdown": "# Title" }' 0.0.0.0:3000/api/render/markdown
  // chrome

  const key = generate({ length: 12, numbers: true })
  db[ key ] = req.body.markdown
  const siteUrl = `https://gcp-markdown.alfreduc.com/page/${key}`
  //const siteUrl = `http://127.0.0.1:3000/page/${key}`
  const imageDir = 'screenshot'
  const imageUrl = `https://gcp-markdown.alfreduc.com/${imageDir}/${key}.png`
  //const imageUrl = `http://127.0.0.1:3000/${imageDir}/${key}.png`
  const imagePath = path.resolve( imageDir, key + '.png' )
  getScreenshot( siteUrl, imagePath )
  .then( () => res.send({ imageUrl, siteUrl }))
  .catch( (err) => res.send( `${key} :: ${err}` ))
})

app.get('/page/:key', (req, res) => {
  const { key } = req.params
  res.setHeader('Content-Type', 'text/html')
  res.send( generateHtml( marked( db[key] || '# undefined' )))
})

app.listen( 3000 )
