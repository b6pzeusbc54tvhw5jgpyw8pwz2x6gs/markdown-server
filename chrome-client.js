import CDP from 'chrome-remote-interface'
import { Promise } from 'when'
import tracer from 'tracer'
import fs from 'fs'

const level = process.env.LOG_LEVEL || 'warn'
const tracerOptions = process.env.DEBUG === 'true' ? { level } : { format: "{{message}}", level }
const logger = tracer.colorConsole( tracerOptions )

const viewportWidth = 840
const viewportHeight = 490

export const getScreenshot = (url,imagePath) => new Promise( (resolve,reject) => {

  const options = {
    host: 'headless-chrome',
    port: 9222,
  }
  CDP( options, async function(client) {

    // Extract used DevTools domains.
    const {DOM, Emulation, Network, Page/*, Runtime*/} = client

    // Enable events on domains we are interested in.
    await Page.enable()
    await DOM.enable()
    await Network.enable()

    // Set up viewport resolution, etc.
    const deviceMetrics = {
      width: viewportWidth,
      height: viewportHeight,
      deviceScaleFactor: 0,
      mobile: false,
      fitWindow: false,
    }
    await Emulation.setDeviceMetricsOverride(deviceMetrics)
    await Emulation.setVisibleSize({width: viewportWidth, height: viewportHeight})

    // Navigate to target page
    //await Page.navigate({url: `${monitoringWebUrl}/${yyyymmdd}` })
    await Page.navigate({ url })

      // Wait for page load event to take screenshot
    Page.loadEventFired(async () => {
      // If the `full` CLI option was passed, we need to measure the height of
      // the rendered page and use Emulation.setVisibleSize
      if ( false ) {
        const {root: {nodeId: documentNodeId}} = await DOM.getDocument()
        const {nodeId: bodyNodeId} = await DOM.querySelector({
          selector: 'body',
          nodeId: documentNodeId,
        })
        const {model: {height}} = await DOM.getBoxModel({nodeId: bodyNodeId})

        await Emulation.setVisibleSize({width: viewportWidth, height: height})
        // This forceViewport call ensures that content outside the viewport is
        // rendered, otherwise it shows up as grey. Possibly a bug?
        await Emulation.forceViewport({x: 0, y: 0, scale: 1})
      }

      setTimeout(async function() {
        const format = 'png'
        const screenshot = await Page.captureScreenshot({format})

        const buffer = new Buffer(screenshot.data, 'base64')
        fs.writeFile( imagePath, buffer, 'base64', function(err) {
          if (err) {
            logger.error(err)
            reject( err )
          } else {
            logger.log( imagePath + ' Screenshot saved')
            resolve({ result: 'SUCCESS' })
          }
          client.close()
        })

      }, 3000)
    })

  }).on('error', err => {
    logger.error('Cannot connect to browser:', err)
    reject('Cannot connect to browser:'+ err)
  })
})
