FROM b6pzeusbc54tvhw5jgpyw8pwz2x6gs/ubuntu16-for-gitlab-runner:v0.2.4

COPY . /root/app
WORKDIR /root/app
RUN mkdir screenshot
CMD ["yarn","start"]
