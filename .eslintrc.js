module.exports = {
  parser: "babel-eslint",
  plugins: ["import"],
  ecmaFeatures: {
    ecamVersion: 6,
    templateStrings: true,
    modules: true,
    classes: true,
    arrowFunctions: true,
    blockBindings: true,
  },
  env: {
    "node": true,
    "es6": true,
  },
  "rules": {
    // "semi": [ 1, "never" ],
    // "no-use-before-define": [2, "nofunc"],
    // "no-param-reassign": 0

    "semi": [ 1, "never" ],
    "prefer-const": [2, {"ignoreReadBeforeAssign": false }],
    "no-use-before-define": 0,
    "no-unused-expressions": [2, { "allowShortCircuit": true }],
    "no-unused-vars": [1, {"vars": "all", "args": "none"}],
    "no-trailing-spaces": ["warn"],
    "no-console": ["error"],
    "indent": ["warn", 2,{ "SwitchCase": 0 } ],
    "comma-dangle": ["warn", "always-multiline"],
    "no-mixed-spaces-and-tabs": 1,
    "no-cond-assign": 1,
    "no-unreachable": 1,
    "block-scoped-var": 2,
    "no-redeclare": 2,
    "no-undef": 2,
    "no-const-assign": 2,
    "import/export": 2,
    "no-var": 1,
    "arrow-spacing": [1, { "before": true, "after": true }],
  },
  globals: {
    "__DEV__": false,
  },
}

